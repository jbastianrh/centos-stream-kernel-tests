# storage/block/RHEL_22151_loop_dio_failed

Storage: loop/dio IO failed in case of being over block device

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
